// test_hid.cpp 2015/6/20
#ifdef TEST_HID
#include "mbed.h"
#include "USBHID.h"
#include "STM32_USB48MHz.h"
#include "mytrace.h"
 
//This report will contain data to be sent
HID_REPORT send_report;
HID_REPORT recv_report;
 
RawSerial pc(USBTX,USBRX);
DigitalOut led1(LED1);
InterruptIn button(USER_BUTTON);

void button_callback() {
    led1 = !led1;
    TRACE_VIEW();
}

int main(void) {
    STM32_HSI_USB48MHz();

    pc.baud(115200);
    pc.printf("%s\n", __FILE__);    
    button.fall(button_callback);

    USBHID hid(8, 8);

    send_report.length = 8;
 
    while (1) {
        //Fill the report
        pc.printf("send: ");
        for (int i = 0; i < send_report.length; i++) {
            send_report.data[i] = rand() & 0xff;
            printf("%d ", send_report.data[i]);
        }
        pc.printf("\r\n");
            
        //Send the report
        hid.send(&send_report);
        
        //try to read a msg
        if(hid.readNB(&recv_report)) {
            pc.printf("recv: ");
            for(int i = 0; i < recv_report.length; i++) {
                pc.printf("%d ", recv_report.data[i]);
            }
            pc.printf("\r\n");
        }
        
        wait_ms(1000);
    }
}
#endif // TEST_HID


