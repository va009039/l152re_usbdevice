// mytrace.h 2015/6/8
#pragma once
#include "mbed.h"

#ifdef DEBUG_TRACE

#define TRACE() myt.trace(__func__, __LINE__)
#define TRACE1(A,B) myt.trace1(__func__, __LINE__, A, B)
#define TRACE_VIEW() myt.view(__func__, __LINE__)
#define TRACE_CLEAR() myt.clear()
#define TRACE_ASSERT(A) if(A){}else{myt.assert(__func__, __LINE__,#A);}
#else

#define TRACE() while(0)
#define TRACE1(A,B) while(0)
#define TRACE_VIEW() while(0)
#define TRACE_CLEAR() while(0)
#define TRACE_ASSERT(A) while(0)

#endif

class myTrace {
public:
    myTrace();
    void trace(const char* func, int line);
    void trace1(const char* func, int line, const char* fmt, int val);
    void view(const char* func, int line);
    void assert(const char* func, int line, const char* s);
    void clear() { wpos = 0; }

private:
    Timer t;
    int wpos;
    static const int TRACE_BUF_SIZE = 512;
    struct {
        uint32_t timestamp_ms;
        const char* func;
        uint16_t line;
        const char* fmt;
        int val;
    } buf[TRACE_BUF_SIZE];
};

extern myTrace myt;


