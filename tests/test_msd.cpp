#ifdef TEST_MSD

#include "mbed.h"
#include "USBMSD_Ram.h"
#include "mytrace.h"
#include "STM32_USB48MHz.h"

RawSerial pc(USBTX,USBRX);
DigitalOut led1(LED1);
InterruptIn button(USER_BUTTON);

void button_callback() {
    led1 = !led1;
    TRACE_VIEW();
}

int main() {
    STM32_HSI_USB48MHz();

    pc.baud(115200);
    pc.printf("%s\n", __FILE__);
    button.fall(button_callback);

    USBMSD_Ram sd;

    while(1);
}

#endif

