// test_pcd_mouse3.cpp 2015/6/8
#ifdef TEST_PCD_MOUSE3

#include <algorithm>
#include "mbed.h"
#include "mytrace.h"

#include "USBEndpoints.h"
#include "USBDescriptor.h"
#include "USBDevice_Types.h"

#include "USBHID_Types.h"

DigitalOut led1(LED1);
DigitalIn sw(USER_BUTTON);
RawSerial pc(USBTX,USBRX);

enum MOUSE_TYPE {
    ABS_MOUSE,
    REL_MOUSE,
};

class MyUSBMouse {
public:
    MyUSBMouse(MOUSE_TYPE mouse_type = REL_MOUSE, uint16_t vendor_id = 0x1234, uint16_t product_id = 0x0001, uint16_t product_release = 0x0001);
    bool configured(void) { return device.state == CONFIGURED; }
    bool write(uint8_t endpoint, uint8_t * buffer, uint32_t size, uint32_t maxSize);

    bool USBCallback_setConfiguration(uint8_t configuration);

    /* Endpoint 0 */
    void EP0setup(uint8_t *buffer);
    void EP0write(uint8_t *buffer, uint32_t size);

    /* Other endpoints */
    EP_STATUS endpointWrite(uint8_t endpoint, uint8_t *data, uint32_t size);
    EP_STATUS endpointWriteResult(uint8_t endpoint);
    bool realiseEndpoint(uint8_t endpoint, uint32_t maxPacket, uint32_t options);

private:
    uint8_t* deviceDesc();
    uint8_t* stringLangidDesc();
    uint8_t* stringImanufacturerDesc();
    uint8_t* stringIserialDesc();
    uint8_t* stringIConfigurationDesc();
    uint8_t* stringIinterfaceDesc();
    uint8_t* stringIproductDesc();
    uint16_t reportLength;
    uint8_t* reportDesc();
    uint16_t reportDescLength();
    uint8_t* configurationDesc();

    uint16_t VENDOR_ID;
    uint16_t PRODUCT_ID;
    uint16_t PRODUCT_RELEASE;
    void decodeSetupPacket(uint8_t *data, SETUP_PACKET *packet);
    CONTROL_TRANSFER transfer;
    USB_DEVICE device;

    HID_REPORT outputReport;

public: // from HAL_PCD_*
    void SetupStageCallback();
    void DataInStageCallback(uint8_t epnum);
    void DataOutStageCallback(uint8_t epnum);
    void ResetCallback();
};

static volatile int epComplete = 0; // bit 0:EP0IN, 1:EP0OUT, 2:EP1_OUT, 3:EP1_IN
static PCD_HandleTypeDef hpcd_USB_FS;

extern "C" void USB_LP_IRQHandler(void) {
    HAL_PCD_IRQHandler(&hpcd_USB_FS);
}

void HAL_PCD_MspInit(PCD_HandleTypeDef* hpcd) {
    TRACE_ASSERT(hpcd->Instance == USB);
    __USB_CLK_ENABLE();
    HAL_NVIC_SetPriority(USB_LP_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(USB_LP_IRQn);
}

static void MX_USB_DEVICE_Init(void) {
    TRACE();

    hpcd_USB_FS.Instance = USB;
    hpcd_USB_FS.Init.dev_endpoints = 8;
    hpcd_USB_FS.Init.speed = PCD_SPEED_FULL;
    hpcd_USB_FS.Init.ep0_mps = DEP0CTL_MPS_8;
    hpcd_USB_FS.Init.phy_itface = PCD_PHY_EMBEDDED;
    hpcd_USB_FS.Init.Sof_enable = DISABLE;
    hpcd_USB_FS.Init.low_power_enable = DISABLE;
    hpcd_USB_FS.Init.battery_charging_enable = DISABLE;
    HAL_PCD_Init(&hpcd_USB_FS);

    HAL_PCDEx_PMAConfig(&hpcd_USB_FS , 0x00 , PCD_SNG_BUF, 0x18);
    HAL_PCDEx_PMAConfig(&hpcd_USB_FS , 0x80 , PCD_SNG_BUF, 0x58);
    HAL_PCDEx_PMAConfig(&hpcd_USB_FS , 0x81 , PCD_SNG_BUF, 0x100);

    HAL_PCD_Start(&hpcd_USB_FS);
}

MyUSBMouse::MyUSBMouse(MOUSE_TYPE mouse_type, uint16_t vendor_id, uint16_t product_id, uint16_t product_release) {
    VENDOR_ID = vendor_id;
    PRODUCT_ID = product_id;
    PRODUCT_RELEASE = product_release;

    /* Set initial device state */
    device.state = POWERED;
    device.configuration = 0;
    device.suspended = false;

    hpcd_USB_FS.pData = this;
    MX_USB_DEVICE_Init();
}

bool MyUSBMouse::write(uint8_t endpoint, uint8_t * buffer, uint32_t size, uint32_t maxSize) {
    if (size > maxSize) {
        return false;
    }
    if(!configured()) {
        return false;
    }
    /* Send report */
    EP_STATUS result = endpointWrite(endpoint, buffer, size);
    if (result != EP_PENDING) {
        return false;
    }
    /* Wait for completion */
    do {
        result = endpointWriteResult(endpoint);
    } while ((result == EP_PENDING) && configured());
    return (result == EP_COMPLETED);
}

bool MyUSBMouse::USBCallback_setConfiguration(uint8_t configuration) {
    TRACE1("configuration=%d", configuration);
    if (configuration == 1) {
        device.configuration = configuration;
        device.state = CONFIGURED;
        realiseEndpoint(EPINT_IN, MAX_PACKET_SIZE_EPINT, 0);
        return true;
    }
    return false;
}

void MyUSBMouse::EP0setup(uint8_t *buffer) {
    memcpy(buffer, hpcd_USB_FS.Setup, 8);
}

void MyUSBMouse::EP0write(uint8_t *buffer, uint32_t size) {
    endpointWrite(EP0IN, buffer, size);
}

EP_STATUS MyUSBMouse::endpointWrite(uint8_t endpoint, uint8_t *data, uint32_t size) {
    TRACE1("endpoint=%d", endpoint);
    TRACE1("size=%d", size);
    HAL_PCD_EP_Transmit(&hpcd_USB_FS, endpoint>>1, data, size);
    epComplete &= ~(1 << endpoint);
    return EP_PENDING;
}

EP_STATUS MyUSBMouse::endpointWriteResult(uint8_t endpoint) {
    //TRACE1("endpoint=%d", endpoint);
    if (epComplete & (1 << endpoint)) {
        epComplete &= ~(1 << endpoint);
        return EP_COMPLETED;
    }
    return EP_PENDING;
}

bool MyUSBMouse::realiseEndpoint(uint8_t endpoint, uint32_t maxPacket, uint32_t options) {
    TRACE1("endpoint=%d", endpoint);
    PCD_HandleTypeDef *hpcd = &hpcd_USB_FS;
    switch(endpoint) {
        case EP0IN:
            HAL_PCD_EP_Open(hpcd, 0x80, maxPacket, PCD_EP_TYPE_CTRL);
            break;
        case EP0OUT:
            HAL_PCD_EP_Open(hpcd, 0x00, maxPacket, PCD_EP_TYPE_CTRL);
            break;
        case EPINT_IN:
            HAL_PCD_EP_Open(hpcd, 0x81, maxPacket, PCD_EP_TYPE_INTR);
            break;
        case EPINT_OUT:
            HAL_PCD_EP_Open(hpcd, 0x01, maxPacket, PCD_EP_TYPE_INTR);
            break;
        case EPBULK_IN:
            HAL_PCD_EP_Open(hpcd, 0x82, maxPacket, PCD_EP_TYPE_BULK);
            break;
        case EPBULK_OUT:
            HAL_PCD_EP_Open(hpcd, 0x02, maxPacket, PCD_EP_TYPE_BULK);
            break;
        case EPISO_IN:
            HAL_PCD_EP_Open(hpcd, 0x83, maxPacket, PCD_EP_TYPE_ISOC);
            break;
        case EPISO_OUT:
            HAL_PCD_EP_Open(hpcd, 0x03, maxPacket, PCD_EP_TYPE_ISOC);
            break;
        default:
            TRACE_ASSERT(0);
            break;
    }
    return true;
}

uint8_t* MyUSBMouse::deviceDesc() {
    static uint8_t deviceDescriptor[] = {
        DEVICE_DESCRIPTOR_LENGTH,       /* bLength */
        DEVICE_DESCRIPTOR,              /* bDescriptorType */
        LSB(USB_VERSION_2_0),           /* bcdUSB (LSB) */
        MSB(USB_VERSION_2_0),           /* bcdUSB (MSB) */
        0x00,                           /* bDeviceClass */
        0x00,                           /* bDeviceSubClass */
        0x00,                           /* bDeviceprotocol */
        MAX_PACKET_SIZE_EP0,            /* bMaxPacketSize0 */
        (uint8_t)(LSB(VENDOR_ID)),                 /* idVendor (LSB) */
        (uint8_t)(MSB(VENDOR_ID)),                 /* idVendor (MSB) */
        (uint8_t)(LSB(PRODUCT_ID)),                /* idProduct (LSB) */
        (uint8_t)(MSB(PRODUCT_ID)),                /* idProduct (MSB) */
        (uint8_t)(LSB(PRODUCT_RELEASE)),           /* bcdDevice (LSB) */
        (uint8_t)(MSB(PRODUCT_RELEASE)),           /* bcdDevice (MSB) */
        STRING_OFFSET_IMANUFACTURER,    /* iManufacturer */
        STRING_OFFSET_IPRODUCT,         /* iProduct */
        STRING_OFFSET_ISERIAL,          /* iSerialNumber */
        0x01                            /* bNumConfigurations */
    };
    return deviceDescriptor;
}

uint8_t* MyUSBMouse::stringLangidDesc() {
    static uint8_t stringLangidDescriptor[] = {
        0x04,               /*bLength*/
        STRING_DESCRIPTOR,  /*bDescriptorType 0x03*/
        0x09,0x04,          /*bString Lang ID - 0x0409 - English*/
    };
    return stringLangidDescriptor;
}

uint8_t* MyUSBMouse::stringImanufacturerDesc() {
    static uint8_t stringImanufacturerDescriptor[] = {
        0x12,                                            /*bLength*/
        STRING_DESCRIPTOR,                               /*bDescriptorType 0x03*/
        'm',0,'b',0,'e',0,'d',0,'.',0,'o',0,'r',0,'g',0, /*bString iManufacturer - mbed.org*/
    };
    return stringImanufacturerDescriptor;
}

uint8_t* MyUSBMouse::stringIserialDesc() {
    static uint8_t stringIserialDescriptor[] = {
        0x16,                                                           /*bLength*/
        STRING_DESCRIPTOR,                                              /*bDescriptorType 0x03*/
        '0',0,'1',0,'2',0,'3',0,'4',0,'5',0,'6',0,'7',0,'8',0,'9',0,    /*bString iSerial - 0123456789*/
    };
    return stringIserialDescriptor;
}

uint8_t* MyUSBMouse::stringIConfigurationDesc() {
    static uint8_t stringIconfigurationDescriptor[] = {
        0x06,               /*bLength*/
        STRING_DESCRIPTOR,  /*bDescriptorType 0x03*/
        '0',0,'1',0,        /*bString iConfiguration - 01*/
    };
    return stringIconfigurationDescriptor;
}

uint8_t* MyUSBMouse::stringIinterfaceDesc() {
    static uint8_t stringIinterfaceDescriptor[] = {
        0x08,               //bLength
        STRING_DESCRIPTOR,  //bDescriptorType 0x03
        'H',0,'I',0,'D',0,  //bString iInterface - HID
    };
    return stringIinterfaceDescriptor;
}

uint8_t* MyUSBMouse::stringIproductDesc() {
    static uint8_t stringIproductDescriptor[] = {
        0x16,                                                       //bLength
        STRING_DESCRIPTOR,                                          //bDescriptorType 0x03
        'H',0,'I',0,'D',0,' ',0,'D',0,'E',0,'V',0,'I',0,'C',0,'E',0 //bString iProduct - HID device
    };
    return stringIproductDescriptor;
}

uint8_t* MyUSBMouse::reportDesc() {
        static uint8_t reportDescriptor[] = {
            USAGE_PAGE(1),      0x01,       // Genric Desktop
            USAGE(1),           0x02,       // Mouse
            COLLECTION(1),      0x01,       // Application
            USAGE(1),           0x01,       // Pointer
            COLLECTION(1),      0x00,       // Physical

            REPORT_COUNT(1),    0x03,
            REPORT_SIZE(1),     0x01,
            USAGE_PAGE(1),      0x09,       // Buttons
            USAGE_MINIMUM(1),       0x1,
            USAGE_MAXIMUM(1),       0x3,
            LOGICAL_MINIMUM(1),     0x00,
            LOGICAL_MAXIMUM(1),     0x01,
            INPUT(1),           0x02,
            REPORT_COUNT(1),    0x01,
            REPORT_SIZE(1),     0x05,
            INPUT(1),           0x01,

            REPORT_COUNT(1),    0x03,
            REPORT_SIZE(1),     0x08,
            USAGE_PAGE(1),      0x01,
            USAGE(1),           0x30,       // X
            USAGE(1),           0x31,       // Y
            USAGE(1),           0x38,       // scroll
            LOGICAL_MINIMUM(1),     0x81,
            LOGICAL_MAXIMUM(1),     0x7f,
            INPUT(1),           0x06,       // Relative data

            END_COLLECTION(0),
            END_COLLECTION(0),
        };
    reportLength = sizeof(reportDescriptor);
    return reportDescriptor;
}

uint16_t MyUSBMouse::reportDescLength() {
    reportDesc();
    return reportLength;
}

#define DEFAULT_CONFIGURATION (1)
#define TOTAL_DESCRIPTOR_LENGTH ((1 * CONFIGURATION_DESCRIPTOR_LENGTH) \
                               + (1 * INTERFACE_DESCRIPTOR_LENGTH) \
                               + (1 * HID_DESCRIPTOR_LENGTH) \
                               + (2 * ENDPOINT_DESCRIPTOR_LENGTH))

uint8_t* MyUSBMouse::configurationDesc() {
    static uint8_t configurationDescriptor[] = {
        CONFIGURATION_DESCRIPTOR_LENGTH,// bLength
        CONFIGURATION_DESCRIPTOR,       // bDescriptorType
        LSB(TOTAL_DESCRIPTOR_LENGTH),   // wTotalLength (LSB)
        MSB(TOTAL_DESCRIPTOR_LENGTH),   // wTotalLength (MSB)
        0x01,                           // bNumInterfaces
        DEFAULT_CONFIGURATION,          // bConfigurationValue
        0x00,                           // iConfiguration
        C_RESERVED | C_SELF_POWERED,    // bmAttributes
        C_POWER(0),                     // bMaxPowerHello World from Mbed

        INTERFACE_DESCRIPTOR_LENGTH,    // bLength
        INTERFACE_DESCRIPTOR,           // bDescriptorType
        0x00,                           // bInterfaceNumber
        0x00,                           // bAlternateSetting
        0x02,                           // bNumEndpoints
        HID_CLASS,                      // bInterfaceClass
        1,                              // bInterfaceSubClass
        2,                              // bInterfaceProtocol (mouse)
        0x00,                           // iInterface

        HID_DESCRIPTOR_LENGTH,          // bLength
        HID_DESCRIPTOR,                 // bDescriptorType
        LSB(HID_VERSION_1_11),          // bcdHID (LSB)
        MSB(HID_VERSION_1_11),          // bcdHID (MSB)
        0x00,                           // bCountryCode
        0x01,                           // bNumDescriptors
        REPORT_DESCRIPTOR,              // bDescriptorType
        (uint8_t)(LSB(reportDescLength())),        // wDescriptorLength (LSB)
        (uint8_t)(MSB(reportDescLength())),        // wDescriptorLength (MSB)

        ENDPOINT_DESCRIPTOR_LENGTH,     // bLength
        ENDPOINT_DESCRIPTOR,            // bDescriptorType
        PHY_TO_DESC(EPINT_IN),          // bEndpointAddress
        E_INTERRUPT,                    // bmAttributes
        LSB(MAX_PACKET_SIZE_EPINT),     // wMaxPacketSize (LSB)
        MSB(MAX_PACKET_SIZE_EPINT),     // wMaxPacketSize (MSB)
        1,                              // bInterval (milliseconds)

        ENDPOINT_DESCRIPTOR_LENGTH,     // bLength
        ENDPOINT_DESCRIPTOR,            // bDescriptorType
        PHY_TO_DESC(EPINT_OUT),         // bEndpointAddress
        E_INTERRUPT,                    // bmAttributes
        LSB(MAX_PACKET_SIZE_EPINT),     // wMaxPacketSize (LSB)
        MSB(MAX_PACKET_SIZE_EPINT),     // wMaxPacketSize (MSB)
        1,                              // bInterval (milliseconds)
    };
    return configurationDescriptor;
}

void MyUSBMouse::decodeSetupPacket(uint8_t *data, SETUP_PACKET *packet) {
    /* Fill in the elements of a SETUP_PACKET structure from raw data */
    packet->bmRequestType.dataTransferDirection = (data[0] & 0x80) >> 7;
    packet->bmRequestType.Type = (data[0] & 0x60) >> 5;
    packet->bmRequestType.Recipient = data[0] & 0x1f;
    packet->bRequest = data[1];
    packet->wValue = (data[2] | (uint16_t)data[3] << 8);
    packet->wIndex = (data[4] | (uint16_t)data[5] << 8);
    packet->wLength = (data[6] | (uint16_t)data[7] << 8);
}

void MyUSBMouse::SetupStageCallback() {
    PCD_HandleTypeDef *hpcd = &hpcd_USB_FS;
    TRACE1("Setup[0]=%08x", hpcd->Setup[0]);
    TRACE1("Setup[1]=%08x", hpcd->Setup[1]);

    decodeSetupPacket(reinterpret_cast<uint8_t*>(hpcd->Setup), &transfer.setup);
    transfer.ptr = NULL;
    transfer.remaining = 0;
    transfer.direction = 0;
    transfer.zlp = false;
    transfer.notify = false;

    bool success = false;
    if (transfer.setup.bmRequestType.Type == STANDARD_TYPE && 
        transfer.setup.bmRequestType.Recipient == DEVICE_RECIPIENT) {
        switch(transfer.setup.bRequest) {
            case GET_DESCRIPTOR:
                switch(DESCRIPTOR_TYPE(transfer.setup.wValue)) {
                    case DEVICE_DESCRIPTOR:
                        transfer.ptr = deviceDesc();
                        transfer.remaining = deviceDesc()[0];
                        transfer.direction = DEVICE_TO_HOST;
                        success = true;
                        break;
                    case CONFIGURATION_DESCRIPTOR:
                        transfer.ptr = configurationDesc();
                        transfer.remaining = TOTAL_DESCRIPTOR_LENGTH;
                        transfer.direction = DEVICE_TO_HOST;
                        success = true;
                        break;
                    case STRING_DESCRIPTOR:
                        switch(DESCRIPTOR_INDEX(transfer.setup.wValue)) {
                            case STRING_OFFSET_LANGID:
                                transfer.ptr = stringLangidDesc();
                                transfer.remaining = stringLangidDesc()[0];
                                transfer.direction = DEVICE_TO_HOST;
                                success = true;
                                break;
                            case STRING_OFFSET_IPRODUCT:
                                transfer.ptr = stringIproductDesc();
                                transfer.remaining = stringIproductDesc()[0];
                                transfer.direction = DEVICE_TO_HOST;
                                success = true;
                                break;
                            case STRING_OFFSET_ISERIAL:
                                transfer.ptr = stringIserialDesc();
                                transfer.remaining = stringIserialDesc()[0];
                                transfer.direction = DEVICE_TO_HOST;
                                success = true;
                                break;
                            default:
                                TRACE_ASSERT(0);
                                break;
                        }
                        break;
                    case QUALIFIER_DESCRIPTOR: // 6
                        success = true;
                        break;
                    default:
                        TRACE_ASSERT(0);
                        break;
                }
                transfer.remaining = std::min(transfer.remaining, (uint32_t)transfer.setup.wLength);
                TRACE1("transfer.remaining=%d", transfer.remaining);
                break;
            case SET_ADDRESS:
                TRACE1("SET_ADDRESS %d", transfer.setup.wValue);
                HAL_PCD_SetAddress(hpcd, transfer.setup.wValue);
                transfer.direction = DEVICE_TO_HOST;
                success = true;
                break;
            case SET_CONFIGURATION:
                USBCallback_setConfiguration(transfer.setup.wValue);
                transfer.direction = DEVICE_TO_HOST;
                success = true;
                break;
            default:
                TRACE_ASSERT(0);
                break;
        }
    } else if (transfer.setup.bmRequestType.Type == STANDARD_TYPE && 
               transfer.setup.bmRequestType.Recipient == INTERFACE_RECIPIENT) {
        switch(transfer.setup.bRequest) {
            case GET_DESCRIPTOR:
                switch(DESCRIPTOR_TYPE(transfer.setup.wValue)) {
                    case REPORT_DESCRIPTOR:
                        transfer.ptr = reportDesc();
                        transfer.remaining = reportDescLength();
                        TRACE1("transfer.remaining=%d", transfer.remaining);
                        TRACE_ASSERT(transfer.remaining < 64);
                        transfer.direction = DEVICE_TO_HOST;
                        success = true;
                        break;
                    default:
                        TRACE_ASSERT(0);
                        break;
                }
                break;
            default:
                TRACE_ASSERT(0);
                break;
        }
    } else if (transfer.setup.bmRequestType.Type == CLASS_TYPE && 
               transfer.setup.bmRequestType.Recipient == INTERFACE_RECIPIENT) {
        switch(transfer.setup.bRequest) {
            case SET_IDLE:
                transfer.direction = DEVICE_TO_HOST;
                success = true;
                break;
            case SET_REPORT:
                // First byte will be used for report ID
                outputReport.data[0] = transfer.setup.wValue & 0xff;
                outputReport.length = transfer.setup.wLength + 1;
                transfer.remaining = sizeof(outputReport.data) - 1;
                transfer.ptr = &outputReport.data[1];
                transfer.direction = HOST_TO_DEVICE;
                transfer.notify = true;
                transfer.direction = DEVICE_TO_HOST;
                success = true;
                break;
            default:
                TRACE_ASSERT(0);
                break;
        }
    } else {
        TRACE1("dataTransferDirection=%d", transfer.setup.bmRequestType.dataTransferDirection);
        TRACE1("Type=%d", transfer.setup.bmRequestType.Type);
        TRACE1("Recipient=%d", transfer.setup.bmRequestType.Recipient);
        TRACE_ASSERT(0);
    }
    TRACE1("success=%d", success);
    if (success) {
        EP0write(transfer.ptr, transfer.remaining);
    }
}

void MyUSBMouse::DataInStageCallback(uint8_t epnum) {
    TRACE1("epnum=%d", epnum);
    PCD_HandleTypeDef *hpcd = &hpcd_USB_FS;
    if (epnum == 0) { // EP0IN
        TRACE1("dataTransferDirection=%d", transfer.setup.bmRequestType.dataTransferDirection);
        if (transfer.setup.bmRequestType.dataTransferDirection == DEVICE_TO_HOST) {
            TRACE();
            HAL_PCD_EP_Receive(hpcd, 0x00, NULL, 0); // next read status stage
        }
    }
    switch(epnum) {
        case 0: epComplete |= 1<<EP0IN; break;
        case 1: epComplete |= 1<<EP1IN; break;
        case 2: epComplete |= 1<<EP2IN; break;
        case 3: epComplete |= 1<<EP3IN; break;
    }
}

void  MyUSBMouse::DataOutStageCallback(uint8_t epnum) {
    TRACE1("epnum=%d", epnum);
    PCD_HandleTypeDef *hpcd = &hpcd_USB_FS;
    if (epnum == 0) { // EP0OUT
        // TODO
    }
    switch(epnum) {
        case 0: epComplete |= 1<<EP0OUT; break;
        case 1: epComplete |= 1<<EP1OUT; break;
        case 2: epComplete |= 1<<EP2OUT; break;
        case 3: epComplete |= 1<<EP3OUT; break;
    }
}

void MyUSBMouse::ResetCallback() {
    TRACE();
    realiseEndpoint(EP0IN, MAX_PACKET_SIZE_EP0, 0);
    realiseEndpoint(EP0OUT, MAX_PACKET_SIZE_EP0, 0);
}

void HAL_PCD_SetupStageCallback(PCD_HandleTypeDef *hpcd) {
    reinterpret_cast<MyUSBMouse*>(hpcd->pData)->SetupStageCallback();
}

void HAL_PCD_DataInStageCallback(PCD_HandleTypeDef *hpcd, uint8_t epnum) {
    reinterpret_cast<MyUSBMouse*>(hpcd->pData)->DataInStageCallback(epnum);
}

void HAL_PCD_DataOutStageCallback(PCD_HandleTypeDef *hpcd, uint8_t epnum) {
    reinterpret_cast<MyUSBMouse*>(hpcd->pData)->DataOutStageCallback(epnum);
}

void HAL_PCD_ResetCallback(PCD_HandleTypeDef *hpcd) {
    reinterpret_cast<MyUSBMouse*>(hpcd->pData)->ResetCallback();
}

void HAL_PCD_SuspendCallback(PCD_HandleTypeDef *hpcd) {
    TRACE();
    if (hpcd->Init.low_power_enable) {
        SCB->SCR |= (uint32_t)((uint32_t)(SCB_SCR_SLEEPDEEP_Msk | SCB_SCR_SLEEPONEXIT_Msk));
    }
}

void HAL_PCDEx_SetConnectionState(PCD_HandleTypeDef *hpcd, uint8_t state) {
    TRACE1("state=%d", state);
    if (state == 1) {
        __HAL_SYSCFG_USBPULLUP_ENABLE();
    } else {
        __HAL_SYSCFG_USBPULLUP_DISABLE();
    } 
}

void L152RE_SystemClock_Config(void) {
    RCC_OscInitTypeDef RCC_OscInitStruct;
    RCC_ClkInitTypeDef RCC_ClkInitStruct;

    __PWR_CLK_ENABLE();

    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.HSICalibrationValue = 16;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
    RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
    RCC_OscInitStruct.PLL.PLLDIV = RCC_PLL_DIV3;
    HAL_RCC_OscConfig(&RCC_OscInitStruct);

    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_SYSCLK;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
    HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1);

    __SYSCFG_CLK_ENABLE();
}

uint32_t L152RE_getUSBclock() {
    RCC_OscInitTypeDef cfg;
    HAL_RCC_GetOscConfig(&cfg);
    MBED_ASSERT(cfg.PLL.PLLState == RCC_PLL_ON);
    uint32_t src = (cfg.PLL.PLLSource == RCC_PLLSOURCE_HSI) ? HSI_VALUE : HSE_VALUE;
    MBED_ASSERT(src == 16000000 || src == 8000000);
    switch(cfg.PLL.PLLMUL) {
        case RCC_PLL_MUL3: src *= 3; break;
        case RCC_PLL_MUL4: src *= 4; break;
        case RCC_PLL_MUL6: src *= 6; break;
        case RCC_PLL_MUL8: src *= 8; break;
        case RCC_PLL_MUL12: src *= 12; break;
        case RCC_PLL_MUL16: src *= 16; break;
        case RCC_PLL_MUL24: src *= 24; break;
        case RCC_PLL_MUL32: src *= 32; break;
        case RCC_PLL_MUL48: src *= 48; break;
    }
    return src / 2;
}

const int MY_BAUD = 115200;

int main() {
    pc.baud(MY_BAUD);
    TRACE1("%s", (int)__FILE__);
    uint32_t usbclk = L152RE_getUSBclock();
    TRACE1("USBclk=%u", usbclk);
    if (usbclk != 48000000) {
        HAL_RCC_DeInit();
        L152RE_SystemClock_Config();
        //SystemCoreClockUpdate();
        pc.baud(MY_BAUD);
    }

    MyUSBMouse mouse;

    TRACE();
    wait_ms(3000);
    TRACE();
    TRACE_VIEW();

    int32_t angle = 0;
    while (1) {
        if (sw == 0) {
            continue;
        }
        const int32_t radius = 10;
        int16_t x = cos((double)angle*3.14/180.0)*radius;
        int16_t y = sin((double)angle*3.14/180.0)*radius;
        while(x != 0 || y != 0) {
            int dx = x;
            dx = std::min(127, dx);
            dx = std::max(-128, dx);
            x -= dx;
            int dy = y;
            dy = std::min(127, dy);
            dy = std::max(-128, dy);
            y -= dy;
            uint8_t report[4] = {0x00,dx, dy, 0x00}; // button, x, y, z
            mouse.write(EPINT_IN, report, sizeof(report), MAX_PACKET_SIZE_EPINT);
        }
        angle += 3;
        wait_ms(5);
    }
}

#endif // TEST_PCD_MOUSE3


