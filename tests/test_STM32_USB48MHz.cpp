// test_STM32_USB48MHz.cpp 2015/6/19
#ifdef TEST_STM32_USB48MHZ
#include "STM32_USB48MHz.h"

DigitalOut led1(LED1);
Serial pc(USBTX,USBRX);

int main() {
    const int BAUD = 115200;
    pc.baud(BAUD);
    pc.printf("%s\n\n", __FILE__);

    uint32_t usbclock = STM32_getUSBclock();
    MBED_ASSERT(usbclock != 48000000);

    Timer t;
    t.reset();
    t.start();
    bool result = false;
    //result = STM32_USB48MHz();
    //result = STM32_HSE_USB48MHz();
    result = STM32_HSI_USB48MHz();
    MBED_ASSERT(result);
    t.stop();

    pc.baud(BAUD);
    pc.printf("%d ms\n", t.read_ms());

    usbclock = STM32_getUSBclock();
    MBED_ASSERT(usbclock == 48000000);

    pc.printf("USBclock=%d MHz\n\n", usbclock);

    while(1) {
        led1 = !led1;
        wait_ms(200);
    }
}
#endif // TEST_STM32_USB48MHZ

