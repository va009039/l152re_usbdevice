// test_pcd3.cpp 2015/6/4
#ifdef TEST_PCD3

#include <algorithm>
#include "mbed.h"
#include "mbed_assert.h"

DigitalOut led1(LED1);
DigitalIn sw(USER_BUTTON);
RawSerial pc(USBTX,USBRX);
Timer timestamp;

enum seq {
    SEQ_INIT = 0,
    SEQ_DATA_IN,
    SEQ_DATA_IN_CONTINE,
    SEQ_DATA_OUT,
    SEQ_STATUS_IN,
    SEQ_STATUS_OUT,
};
int seq = SEQ_INIT;
int dev_addr = 0;
struct data {
    uint8_t* p;
    size_t size;
    void init(uint8_t* p_, size_t size_) {
        p = p_;
        size = size_;
    }
} data;


#define PCD_DBG(...) do{_dbg_func<RawSerial,Timer>(pc,timestamp,__func__,__LINE__);pc.printf(__VA_ARGS__);pc.puts("\n");} while(0);
#define PCD_PR(...) do{pc.printf("[@%d] ",__LINE__);pc.printf(__VA_ARGS__);pc.puts("\n");} while(0);
#define PCD_HEX(A,B) do{_dbg_hex<RawSerial>(pc,A,B);} while(0);
#define PCD_ASSERT(A) while(!(A)){_dbg_func<RawSerial,Timer>(pc,timestamp,__func__,__LINE__);pc.puts(#A);exit(1);}

template<class SERIAL_T, class TIMER_T>
void _dbg_func(SERIAL_T& pc, TIMER_T& t, const char* func, int line) {
    pc.printf("[%ums %s@%d] ",t.read_ms(), func, line);
}

template<class SERIAL_T>
void _dbg_hex(SERIAL_T& pc, const uint8_t* buf, int size) {
    for(int i = 0; i < size; i++) {
        pc.printf("%02x ", buf[i]);
        if ((i%16) == 15) {
            pc.puts("\n");
        }
    }
    pc.puts("\n");
}

PCD_HandleTypeDef hpcd_USB_FS;

extern "C" void USB_LP_IRQHandler(void) {
    HAL_PCD_IRQHandler(&hpcd_USB_FS);
}

void HAL_PCD_MspInit(PCD_HandleTypeDef* hpcd) {
    PCD_DBG("hpcd=%p", hpcd);
    if(hpcd->Instance == USB) {
        __USB_CLK_ENABLE();
        HAL_NVIC_SetPriority(USB_LP_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(USB_LP_IRQn);
    }
}

void HAL_PCD_MspDeInit(PCD_HandleTypeDef* hpcd) {
    PCD_DBG("hpcd=%p", hpcd);
    if(hpcd->Instance == USB) {
      __USB_CLK_DISABLE();
      HAL_NVIC_DisableIRQ(USB_LP_IRQn);
  }
}

void HAL_PCD_SetupStageCallback(PCD_HandleTypeDef *hpcd) {
    PCD_DBG("hpcd=%p", hpcd);
    PCD_HEX((uint8_t*)hpcd->Setup, 8);
    uint8_t mbRequestType = hpcd->Setup[0] & 0xff;
    uint8_t bRequest = hpcd->Setup[0]>>8 & 0xff;
    uint16_t wValue = hpcd->Setup[0]>>16;
    uint16_t wIndex = hpcd->Setup[1] & 0xffff;
    int wLength = hpcd->Setup[1]>>16;
    data.init(NULL, 0);
    if (mbRequestType == 0x80 && bRequest == 6) {
        uint8_t desc_type = wValue>>8;
        //PCD_DBG("GET_DESCRIPTOR %d, wLength=%d", desc_type, wLength);
        if (desc_type == 1) { // device
            static uint8_t desc[18] = {
                0x12,0x01,0x00,0x02,0x00,0x00,0x00,0x40,
                0x83,0x04,0x2b,0x57,0x00,0x02,0x01,0x02,
                0x03,0x01};
            data.init(desc, sizeof(desc));
        } else if (desc_type == 2) { // configuration 
            static uint8_t desc[] = {
                0x09,0x02,0x22,0x00,0x01,0x01,0x00,0xe0,0x32,
                0x09,0x04,0x00,0x00,0x01,0x03,0x01,0x02,0x00,
                0x09,0x21,0x11,0x01,0x00,0x01,0x22,0x4a,0x00,
                0x07,0x05,0x81,0x03,0x04,0x00,0x0a};
            data.init(desc, sizeof(desc));
        } else if (desc_type == 3) { // string
            if ((wValue&0xff) == 0) {
                static uint8_t desc[] = {0x04,0x03,0x09,0x04};
                data.init(desc, sizeof(desc));
            } else if ((wValue&0xff) == 2) {
                static uint8_t desc[] = {
                    0x2c,0x03,0x53,0x00,0x54,0x00,0x4d,0x00,
                    0x33,0x00,0x32,0x00,0x20,0x00,0x48,0x00,
                    0x75,0x00,0x6d,0x00,0x61,0x00,0x6e,0x00,
                    0x20,0x00,0x69,0x00,0x6e,0x00,0x74,0x00,
                    0x65,0x00,0x72,0x00,0x66,0x00,0x61,0x00,
                    0x63,0x00,0x65,0x00};
                data.init(desc, sizeof(desc));
            } else if ((wValue&0xff) == 3) {
                static uint8_t desc[] = {
                    0x1a,0x03,0x30,0x00,0x30,0x00,0x30,0x00,
                    0x30,0x00,0x30,0x00,0x30,0x00,0x30,0x00,
                    0x30,0x00,0x30,0x00,0x30,0x00,0x31,0x00,
                    0x41,0x00};
                data.init(desc, sizeof(desc));
            } else {
                PCD_HEX((uint8_t*)hpcd->Setup, 8);
                PCD_ASSERT(0);
            }
        } else if (desc_type == 6) {
            ; // pass
        } else {
            PCD_HEX((uint8_t*)hpcd->Setup, 8);
            PCD_ASSERT(0);
        }
        int size = std::min(wLength, (int)data.size);
        PCD_DBG("size=%d", size);
        PCD_HEX(data.p, size);
        HAL_PCD_EP_Transmit(hpcd, 0x00, data.p, size);
        seq = SEQ_DATA_IN;
    } else if (mbRequestType == 0x00 && bRequest == 5) { //SET_ADDRESS
        dev_addr = wValue;
        //PCD_DBG("SET_ADDRESS %d", dev_addr);
        HAL_PCD_SetAddress(hpcd, dev_addr);
        HAL_PCD_EP_Transmit(hpcd, 0x00, NULL, 0); // status stage
        seq = SEQ_STATUS_IN;
    } else if (mbRequestType == 0x00 && bRequest == 9) { //SET_CONFIGURATITON
        HAL_PCD_EP_Transmit(hpcd, 0x00, NULL, 0); // status stage
        seq = SEQ_STATUS_IN;
    } else if (mbRequestType == 0x21 && bRequest == 10) { // HID descriptor
        HAL_PCD_EP_Transmit(hpcd, 0x00, NULL, 0); // status stage
        seq = SEQ_STATUS_IN;
    } else if (mbRequestType == 0x81 && bRequest == 6) { // report descriptor
        static uint8_t desc[] = {
            0x05,0x01,0x09,0x02,0xa1,0x01,0x09,0x01,
            0xa1,0x00,0x05,0x09,0x19,0x01,0x29,0x03,
            0x15,0x00,0x25,0x01,0x95,0x03,0x75,0x01,
            0x81,0x02,0x95,0x01,0x75,0x05,0x81,0x01,
            0x05,0x01,0x09,0x30,0x09,0x31,0x09,0x38,
            0x15,0x81,0x25,0x7f,0x75,0x08,0x95,0x03,
            0x81,0x06,0xc0,0x09,0x3c,0x05,0xff,0x09,
            0x01,0x15,0x00,0x25,0x01,0x75,0x01,0x95,
            0x02,0xb1,0x22,0x75,0x06,0x95,0x01,0xb1,
            0x01,0xc0,};
        data.init(desc, sizeof(desc));
        if (data.size < 64) {
            PCD_DBG("size=%d", data.size);
            PCD_HEX(data.p, data.size);
            HAL_PCD_EP_Transmit(hpcd, 0x00, data.p, data.size);
            seq = SEQ_DATA_IN;
        } else {
            PCD_DBG("size=%d", 64);
            PCD_HEX(data.p, 64);
            HAL_PCD_EP_Transmit(hpcd, 0x00, data.p, 64);
            seq = SEQ_DATA_IN_CONTINE;
        }
    } else {
        PCD_HEX((uint8_t*)hpcd->Setup, 8);
        PCD_ASSERT(0);
   }
}

void HAL_PCD_DataOutStageCallback(PCD_HandleTypeDef *hpcd, uint8_t epnum) {
    //PCD_DBG("hpcd=%p, epnum=%d, seq=%d", hpcd, epnum, seq);
    if (epnum == 0) {
        if (seq == SEQ_STATUS_OUT) {
            int r = HAL_PCD_EP_GetRxCount(hpcd, 0x00);
            PCD_ASSERT(r == 0);
        } else {
            PCD_ASSERT(0);
        }
    }
}

__IO bool ep1_done = true;

void HAL_PCD_DataInStageCallback(PCD_HandleTypeDef *hpcd, uint8_t epnum) {
    if (epnum == 0) {
        PCD_DBG("hpcd=%p, epnum=%d, seq=%d", hpcd, epnum, seq);
        switch(seq) {
            case SEQ_DATA_IN:
                HAL_PCD_EP_Receive(hpcd, 0x00, NULL, 0); // status stage
                seq = SEQ_STATUS_OUT;
                break;
            case SEQ_DATA_IN_CONTINE:
                PCD_HEX(data.p + 64, data.size - 64);
                HAL_PCD_EP_Transmit(hpcd, 0x00, data.p + 64, data.size - 64);
                seq = SEQ_DATA_IN;
                break;
            case SEQ_STATUS_IN:
                seq = SEQ_INIT;
                break;
            default:
                PCD_ASSERT(0);
        }
    } else {
        //PCD_DBG("hpcd=%p, epnum=%d, seq=%d", hpcd, epnum, seq);
        ep1_done = true;
    }
}

__IO int sof_callback = 0;

void HAL_PCD_SOFCallback(PCD_HandleTypeDef *hpcd) {
    sof_callback++;
}

void HAL_PCD_ResetCallback(PCD_HandleTypeDef *hpcd) {
    PCD_DBG("hpcd=%p", hpcd);
    HAL_PCD_EP_Open(hpcd, 0x00, 64, PCD_EP_TYPE_CTRL);
    HAL_PCD_EP_Open(hpcd, 0x80, 64, PCD_EP_TYPE_CTRL);
    HAL_PCD_EP_Open(hpcd, 0x81, 64, PCD_EP_TYPE_INTR);
}

void HAL_PCD_SuspendCallback(PCD_HandleTypeDef *hpcd) {
    PCD_DBG("hpcd=%p", hpcd);
    if (hpcd->Init.low_power_enable) {
        SCB->SCR |= (uint32_t)((uint32_t)(SCB_SCR_SLEEPDEEP_Msk | SCB_SCR_SLEEPONEXIT_Msk));
    }
}

void HAL_PCD_ResumeCallback(PCD_HandleTypeDef *hpcd) {
    PCD_DBG("hpcd=%p", hpcd);
}

void HAL_PCD_ConnectCallback(PCD_HandleTypeDef *hpcd) {
    PCD_DBG("hpcd=%p", hpcd);
}

void HAL_PCD_DisconnectCallback(PCD_HandleTypeDef *hpcd) {
    PCD_DBG("hpcd=%p", hpcd);
}


void HAL_PCDEx_SetConnectionState(PCD_HandleTypeDef *hpcd, uint8_t state) {
    PCD_DBG("state=%d", state);
    if (state == 1) {
        __HAL_SYSCFG_USBPULLUP_ENABLE();
    } else {
        __HAL_SYSCFG_USBPULLUP_DISABLE();
    } 
}


void L152RE_SystemClock_Config(void) {
    PCD_DBG("");

    RCC_OscInitTypeDef RCC_OscInitStruct;
    RCC_ClkInitTypeDef RCC_ClkInitStruct;

    __PWR_CLK_ENABLE();

    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.HSICalibrationValue = 16;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
    RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
    RCC_OscInitStruct.PLL.PLLDIV = RCC_PLL_DIV3;
    HAL_RCC_OscConfig(&RCC_OscInitStruct);

    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_SYSCLK;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
    HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1);

    __SYSCFG_CLK_ENABLE();
}

uint32_t L152RE_getUSBclock() {
    RCC_OscInitTypeDef cfg;
    HAL_RCC_GetOscConfig(&cfg);
    MBED_ASSERT(cfg.PLL.PLLState == RCC_PLL_ON);
    uint32_t src = (cfg.PLL.PLLSource == RCC_PLLSOURCE_HSI) ? HSI_VALUE : HSE_VALUE;
    MBED_ASSERT(src == 16000000 || src == 8000000);
    switch(cfg.PLL.PLLMUL) {
        case RCC_PLL_MUL3: src *= 3; break;
        case RCC_PLL_MUL4: src *= 4; break;
        case RCC_PLL_MUL6: src *= 6; break;
        case RCC_PLL_MUL8: src *= 8; break;
        case RCC_PLL_MUL12: src *= 12; break;
        case RCC_PLL_MUL16: src *= 16; break;
        case RCC_PLL_MUL24: src *= 24; break;
        case RCC_PLL_MUL32: src *= 32; break;
        case RCC_PLL_MUL48: src *= 48; break;
    }
    return src / 2;
}


void MX_USB_DEVICE_Init(void) {
    PCD_DBG("");

    hpcd_USB_FS.Instance = USB;
    hpcd_USB_FS.Init.dev_endpoints = 8;
    hpcd_USB_FS.Init.speed = PCD_SPEED_FULL;
    hpcd_USB_FS.Init.ep0_mps = DEP0CTL_MPS_8;
    hpcd_USB_FS.Init.phy_itface = PCD_PHY_EMBEDDED;
    hpcd_USB_FS.Init.Sof_enable = DISABLE;
    hpcd_USB_FS.Init.low_power_enable = DISABLE;
    hpcd_USB_FS.Init.battery_charging_enable = DISABLE;
    HAL_PCD_Init(&hpcd_USB_FS);

    HAL_PCDEx_PMAConfig(&hpcd_USB_FS , 0x00 , PCD_SNG_BUF, 0x18);
    HAL_PCDEx_PMAConfig(&hpcd_USB_FS , 0x80 , PCD_SNG_BUF, 0x58);
    HAL_PCDEx_PMAConfig(&hpcd_USB_FS , 0x81 , PCD_SNG_BUF, 0x100);

    HAL_PCD_Start(&hpcd_USB_FS);
}


const int MY_BAUD = 115200;

int main() {
    timestamp.reset();
    timestamp.start();
    pc.baud(MY_BAUD);
    PCD_DBG("%s", __FILE__);
    uint32_t usbclk = L152RE_getUSBclock();
    PCD_DBG("USBclk=%u", usbclk);
    if (usbclk != 48000000) {
        HAL_RCC_DeInit();
        L152RE_SystemClock_Config();
        //SystemCoreClockUpdate();
        pc.baud(MY_BAUD);
    }
    MX_USB_DEVICE_Init();

    for(int n = 0; n < 5; n++) {
        wait_ms(500);
        PCD_DBG("n=%d sof=%d", n, sof_callback);
    }

#if 0
    while(1) {
        if (sw == 0) {
            PCD_DBG("sw=0");
            static uint8_t report[4] = {0x00,0x01,0x01,0x00}; // button, x, y, z
            HAL_PCD_EP_Transmit(&hpcd_USB_FS, 0x81, report, sizeof(report));
        }
        led1 = !led1;
        wait_ms(200);
    }
#endif

    int32_t angle = 0;
    while (1) {
        if (sw == 0) {
            continue;
        }
        const int32_t radius = 10;
        int16_t x = cos((double)angle*3.14/180.0)*radius;
        int16_t y = sin((double)angle*3.14/180.0)*radius;
        while(x != 0 || y != 0) {
            int16_t dx = x;
            if (dx > 127) {
                dx = 127;
            } else if (dx < -128) {
                dx = -128;
            }
            x -= dx;
            int16_t dy = y;
            if (dy > 127) {
                dy = 127;
            } else if (dy < -128) {
                dy = -128;
            }
            y -= dy;
            while(!ep1_done) {
                wait_us(100);
            }
            ep1_done = false;
            uint8_t report[4] = {0x00,dx, dy, 0x00}; // button, x, y, z
            HAL_PCD_EP_Transmit(&hpcd_USB_FS, 0x81, report, sizeof(report));
        }
        angle += 3;
        wait_ms(2);
    }


}

#endif // TEST_PCD3


