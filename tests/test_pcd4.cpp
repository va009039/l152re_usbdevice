// test_pcd4.cpp 2015/6/3
#ifdef TEST_PCD4

#include "mbed.h"
#include "mbed_assert.h"
#include "usb_device.h"

DigitalOut led1(LED1);
RawSerial pc(USBTX,USBRX);
Timer timestamp;



#define PCD_DBG(...) do{_dbg_func<RawSerial,Timer>(pc,timestamp,__func__,__LINE__);pc.printf(__VA_ARGS__);pc.puts("\n");} while(0);
#define PCD_PR(...) do{pc.printf("[@%d] ",__LINE__);pc.printf(__VA_ARGS__);pc.puts("\n");} while(0);
#define PCD_HEX(A,B) do{_dbg_hex<RawSerial>(pc,A,B);} while(0);
#define PCD_ASSERT(A) while(!(A)){_dbg_func<RawSerial,Timer>(pc,timestamp,__func__,__LINE__);pc.puts(#A);exit(1);}

#define DBG(...) do{printf("[%s@%d] ",__func__,__LINE__);printf(__VA_ARGS__);puts("\n");} while(0);


template<class SERIAL_T, class TIMER_T>
void _dbg_func(SERIAL_T& pc, TIMER_T& t, const char* func, int line) {
    pc.printf("[%ums %s@%d] ",t.read_ms(), func, line);
}

template<class SERIAL_T>
void _dbg_hex(SERIAL_T& pc, const uint8_t* buf, int size) {
    for(int i = 0; i < size; i++) {
        pc.printf("%02x ", buf[i]);
        if ((i%16) == 15) {
            pc.puts("\n");
        }
    }
    pc.puts("\n");
}

extern PCD_HandleTypeDef hpcd_USB_FS;
extern "C" void USB_LP_IRQHandler(void) {
  HAL_PCD_IRQHandler(&hpcd_USB_FS);
}

void L152RE_SystemClock_Config(void) {
    PCD_DBG("");

    RCC_OscInitTypeDef RCC_OscInitStruct;
    RCC_ClkInitTypeDef RCC_ClkInitStruct;

    __PWR_CLK_ENABLE();

    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.HSICalibrationValue = 16;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
    RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
    RCC_OscInitStruct.PLL.PLLDIV = RCC_PLL_DIV3;
    HAL_RCC_OscConfig(&RCC_OscInitStruct);

    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_SYSCLK;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
    HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1);

    __SYSCFG_CLK_ENABLE();
}

uint32_t L152RE_getUSBclock() {
    RCC_OscInitTypeDef cfg;
    HAL_RCC_GetOscConfig(&cfg);
    MBED_ASSERT(cfg.PLL.PLLState == RCC_PLL_ON);
    uint32_t src = (cfg.PLL.PLLSource == RCC_PLLSOURCE_HSI) ? HSI_VALUE : HSE_VALUE;
    MBED_ASSERT(src == 16000000 || src == 8000000);
    switch(cfg.PLL.PLLMUL) {
        case RCC_PLL_MUL3: src *= 3; break;
        case RCC_PLL_MUL4: src *= 4; break;
        case RCC_PLL_MUL6: src *= 6; break;
        case RCC_PLL_MUL8: src *= 8; break;
        case RCC_PLL_MUL12: src *= 12; break;
        case RCC_PLL_MUL16: src *= 16; break;
        case RCC_PLL_MUL24: src *= 24; break;
        case RCC_PLL_MUL32: src *= 32; break;
        case RCC_PLL_MUL48: src *= 48; break;
    }
    return src / 2;
}

const int MY_BAUD = 115200;

int main() {
    timestamp.reset();
    timestamp.start();
    pc.baud(MY_BAUD);
    PCD_DBG("%s", __FILE__);
    uint32_t usbclk = L152RE_getUSBclock();
    PCD_DBG("USBclk=%d", usbclk);
    if (usbclk != 48000000) {
        HAL_RCC_DeInit();
        L152RE_SystemClock_Config();
        //SystemCoreClockUpdate();
        pc.baud(MY_BAUD);
    }
    MX_USB_DEVICE_Init();

    PCD_DBG("");
    DBG("");
    while(1) {
        led1 = !led1;
        wait_ms(200);
    }
}

#endif // TEST_PCD4


