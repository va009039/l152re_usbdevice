// test_mouse.cpp 2015/6/20
#ifdef TEST_MOUSE

#include "mbed.h"
#include "USBMouse.h"
#include "STM32_USB48MHz.h"

#include "mytrace.h"

RawSerial pc(USBTX,USBRX);
DigitalOut led1(LED1);
InterruptIn button(USER_BUTTON);

void button_callback() {
    led1 = !led1;
    TRACE_VIEW();
}

int main() {
    STM32_HSI_USB48MHz();

    pc.baud(115200);
    pc.printf("%s\n", __FILE__);

    button.fall(button_callback);

    TRACE();
    TRACE_VIEW();

    USBMouse mouse;

    TRACE_VIEW();
    wait_ms(3000);


    int16_t x = 0;
    int16_t y = 0;
    int32_t radius = 10;
    int32_t angle = 0;

    while (1) {
        x = cos((double)angle*3.14/180.0)*radius;
        y = sin((double)angle*3.14/180.0)*radius;
        
        mouse.move(x, y);
        angle += 3;
        wait_ms(5);
    }
}

#endif

