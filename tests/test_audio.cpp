#ifdef TEST_AUDIO
// https://developer.mbed.org/handbook/USBAudio
// Hello World example for the USBAudio library
 
#include "mbed.h"
#include "USBAudio.h"
#include "mytrace.h"
#include "STM32_USB48MHz.h"
 
// frequency: 48 kHz
#define FREQ 48000
 
// 1 channel: mono
#define NB_CHA 1
 
// length of an audio packet: each ms, we receive 48 * 16bits ->48 * 2 bytes. as there is one channel, the length will be 48 * 2 * 1
#define AUDIO_LENGTH_PACKET 48 * 2 * 1

RawSerial pc(USBTX,USBRX);
DigitalOut led1(LED1);
InterruptIn button(USER_BUTTON);

void button_callback() {
    led1 = !led1;
    TRACE_VIEW();
}

int main() {
    STM32_HSI_USB48MHz();

    pc.baud(115200);
    pc.printf("%s\n", __FILE__);
    button.fall(button_callback);

    // USBAudio
    USBAudio audio(FREQ, NB_CHA);

    int16_t buf[AUDIO_LENGTH_PACKET/2];
    
    while (1) {
        // read an audio packet
        audio.read((uint8_t *)buf);
 
        // print packet received
        pc.printf("recv: ");
        for(int i = 0; i < AUDIO_LENGTH_PACKET/2; i++) {
            pc.printf("%d ", buf[i]);
        }
        pc.printf("\r\n");
    }
}
#endif


