// test_serial.cpp 2015/6/20
#ifdef TEST_SERIAL
// https://developer.mbed.org/handbook/USBSerial
#include "mbed.h"
#include "USBSerial.h"
#include "mytrace.h"
#include "STM32_USB48MHz.h"

RawSerial pc(USBTX,USBRX);
DigitalOut led1(LED1);
InterruptIn button(USER_BUTTON);

void button_callback() {
    led1 = !led1;
    TRACE_VIEW();
}

int main() {
    STM32_HSI_USB48MHz();

    pc.baud(115200);
    pc.printf("%s\n", __FILE__);
    button.fall(button_callback);

    USBSerial serial;
    Timer t;
    t.reset();
    t.start();
    int n = 0;
    while(1) {
        if (t.read_ms() >= 1000) {
            serial.printf("%d I am a virtual serial port\r\n", n++);
            t.reset();
        }
        if (serial.readable()) {
            int c = serial.getc();
            pc.putc(c);
        }
        if (pc.readable()) {
            int c = pc.getc();
            serial.putc(c);
        }
    }
}

#endif // TEST_SERIAL


