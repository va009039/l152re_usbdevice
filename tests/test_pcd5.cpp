// test_pcd5.cpp 2015/6/6
#ifdef TEST_PCD5

#include <algorithm>
#include "mbed.h"
#include "mbed_assert.h"

#include "USBEndpoints.h"
#include "USBDescriptor.h"
#include "USBDevice_Types.h"

#include "USBHID_Types.h"

DigitalOut led1(LED1);
DigitalIn sw(USER_BUTTON);
RawSerial pc(USBTX,USBRX);
Timer timestamp;

enum seq {
    SEQ_INIT = 0,
    SEQ_DATA_IN,
    SEQ_DATA_IN_CONTINE,
    SEQ_DATA_OUT,
    SEQ_STATUS_IN,
    SEQ_STATUS_OUT,
};
int seq = SEQ_INIT;
int dev_addr = 0;
struct data {
    uint8_t* p;
    size_t size;
    void init(uint8_t* p_, size_t size_) {
        p = p_;
        size = size_;
    }
} data;

static volatile int epComplete = 0;
// bit 0...EP1_OUT
// bit 1...EP1_IN

CONTROL_TRANSFER transfer;


#define PCD_DBG(...) do{_dbg_func<RawSerial,Timer>(pc,timestamp,__func__,__LINE__);pc.printf(__VA_ARGS__);pc.puts("\n");} while(0);
#define PCD_PR(...) do{pc.printf("[@%d] ",__LINE__);pc.printf(__VA_ARGS__);pc.puts("\n");} while(0);
#define PCD_HEX(A,B) do{_dbg_hex<RawSerial>(pc,A,B);} while(0);
#define PCD_ASSERT(A) while(!(A)){_dbg_func<RawSerial,Timer>(pc,timestamp,__func__,__LINE__);pc.puts(#A);exit(1);}

template<class SERIAL_T, class TIMER_T>
void _dbg_func(SERIAL_T& pc, TIMER_T& t, const char* func, int line) {
    pc.printf("[%ums %s@%d] ",t.read_ms(), func, line);
}

template<class SERIAL_T>
void _dbg_hex(SERIAL_T& pc, const uint8_t* buf, int size) {
    for(int i = 0; i < size; i++) {
        pc.printf("%02x ", buf[i]);
        if ((i%16) == 15) {
            pc.puts("\n");
        }
    }
    pc.puts("\n");
}

PCD_HandleTypeDef hpcd_USB_FS;

extern "C" void USB_LP_IRQHandler(void) {
    HAL_PCD_IRQHandler(&hpcd_USB_FS);
}

void HAL_PCD_MspInit(PCD_HandleTypeDef* hpcd) {
    PCD_DBG("hpcd=%p", hpcd);
    if(hpcd->Instance == USB) {
        __USB_CLK_ENABLE();
        HAL_NVIC_SetPriority(USB_LP_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(USB_LP_IRQn);
    }
}

void HAL_PCD_MspDeInit(PCD_HandleTypeDef* hpcd) {
    PCD_DBG("hpcd=%p", hpcd);
    if(hpcd->Instance == USB) {
      __USB_CLK_DISABLE();
      HAL_NVIC_DisableIRQ(USB_LP_IRQn);
  }
}

uint16_t VENDOR_ID = 0x1234;
uint16_t PRODUCT_ID = 0x0001;
uint16_t PRODUCT_RELEASE = 0x0001;

// USBDevice.cpp
uint8_t * deviceDesc() {
    static uint8_t deviceDescriptor[] = {
        DEVICE_DESCRIPTOR_LENGTH,       /* bLength */
        DEVICE_DESCRIPTOR,              /* bDescriptorType */
        LSB(USB_VERSION_2_0),           /* bcdUSB (LSB) */
        MSB(USB_VERSION_2_0),           /* bcdUSB (MSB) */
        0x00,                           /* bDeviceClass */
        0x00,                           /* bDeviceSubClass */
        0x00,                           /* bDeviceprotocol */
        MAX_PACKET_SIZE_EP0,            /* bMaxPacketSize0 */
        (uint8_t)(LSB(VENDOR_ID)),                 /* idVendor (LSB) */
        (uint8_t)(MSB(VENDOR_ID)),                 /* idVendor (MSB) */
        (uint8_t)(LSB(PRODUCT_ID)),                /* idProduct (LSB) */
        (uint8_t)(MSB(PRODUCT_ID)),                /* idProduct (MSB) */
        (uint8_t)(LSB(PRODUCT_RELEASE)),           /* bcdDevice (LSB) */
        (uint8_t)(MSB(PRODUCT_RELEASE)),           /* bcdDevice (MSB) */
        STRING_OFFSET_IMANUFACTURER,    /* iManufacturer */
        STRING_OFFSET_IPRODUCT,         /* iProduct */
        STRING_OFFSET_ISERIAL,          /* iSerialNumber */
        0x01                            /* bNumConfigurations */
    };
    return deviceDescriptor;
}

uint8_t * stringLangidDesc() {
    static uint8_t stringLangidDescriptor[] = {
        0x04,               /*bLength*/
        STRING_DESCRIPTOR,  /*bDescriptorType 0x03*/
        0x09,0x04,          /*bString Lang ID - 0x0409 - English*/
    };
    return stringLangidDescriptor;
}

uint8_t * stringImanufacturerDesc() {
    static uint8_t stringImanufacturerDescriptor[] = {
        0x12,                                            /*bLength*/
        STRING_DESCRIPTOR,                               /*bDescriptorType 0x03*/
        'm',0,'b',0,'e',0,'d',0,'.',0,'o',0,'r',0,'g',0, /*bString iManufacturer - mbed.org*/
    };
    return stringImanufacturerDescriptor;
}

uint8_t * stringIserialDesc() {
    static uint8_t stringIserialDescriptor[] = {
        0x16,                                                           /*bLength*/
        STRING_DESCRIPTOR,                                              /*bDescriptorType 0x03*/
        '0',0,'1',0,'2',0,'3',0,'4',0,'5',0,'6',0,'7',0,'8',0,'9',0,    /*bString iSerial - 0123456789*/
    };
    return stringIserialDescriptor;
}

uint8_t * stringIConfigurationDesc() {
    static uint8_t stringIconfigurationDescriptor[] = {
        0x06,               /*bLength*/
        STRING_DESCRIPTOR,  /*bDescriptorType 0x03*/
        '0',0,'1',0,        /*bString iConfiguration - 01*/
    };
    return stringIconfigurationDescriptor;
}

uint8_t * stringIinterfaceDesc() {
    static uint8_t stringIinterfaceDescriptor[] = {
        0x08,               //bLength
        STRING_DESCRIPTOR,  //bDescriptorType 0x03
        'H',0,'I',0,'D',0,  //bString iInterface - HID
    };
    return stringIinterfaceDescriptor;
}

uint8_t * stringIproductDesc() {
    static uint8_t stringIproductDescriptor[] = {
        0x16,                                                       //bLength
        STRING_DESCRIPTOR,                                          //bDescriptorType 0x03
        'H',0,'I',0,'D',0,' ',0,'D',0,'E',0,'V',0,'I',0,'C',0,'E',0 //bString iProduct - HID device
    };
    return stringIproductDescriptor;
}

uint16_t reportLength;
// USBMouse.cpp
uint8_t * reportDesc() {
        static uint8_t reportDescriptor[] = {
            USAGE_PAGE(1),      0x01,       // Genric Desktop
            USAGE(1),           0x02,       // Mouse
            COLLECTION(1),      0x01,       // Application
            USAGE(1),           0x01,       // Pointer
            COLLECTION(1),      0x00,       // Physical

            REPORT_COUNT(1),    0x03,
            REPORT_SIZE(1),     0x01,
            USAGE_PAGE(1),      0x09,       // Buttons
            USAGE_MINIMUM(1),       0x1,
            USAGE_MAXIMUM(1),       0x3,
            LOGICAL_MINIMUM(1),     0x00,
            LOGICAL_MAXIMUM(1),     0x01,
            INPUT(1),           0x02,
            REPORT_COUNT(1),    0x01,
            REPORT_SIZE(1),     0x05,
            INPUT(1),           0x01,

            REPORT_COUNT(1),    0x03,
            REPORT_SIZE(1),     0x08,
            USAGE_PAGE(1),      0x01,
            USAGE(1),           0x30,       // X
            USAGE(1),           0x31,       // Y
            USAGE(1),           0x38,       // scroll
            LOGICAL_MINIMUM(1),     0x81,
            LOGICAL_MAXIMUM(1),     0x7f,
            INPUT(1),           0x06,       // Relative data

            END_COLLECTION(0),
            END_COLLECTION(0),
        };
    reportLength = sizeof(reportDescriptor);
    return reportDescriptor;
}

uint16_t reportDescLength() {
    reportDesc();
    return reportLength;
}

#define DEFAULT_CONFIGURATION (1)
#define TOTAL_DESCRIPTOR_LENGTH ((1 * CONFIGURATION_DESCRIPTOR_LENGTH) \
                               + (1 * INTERFACE_DESCRIPTOR_LENGTH) \
                               + (1 * HID_DESCRIPTOR_LENGTH) \
                               + (2 * ENDPOINT_DESCRIPTOR_LENGTH))

uint8_t * configurationDesc() {
    static uint8_t configurationDescriptor[] = {
        CONFIGURATION_DESCRIPTOR_LENGTH,// bLength
        CONFIGURATION_DESCRIPTOR,       // bDescriptorType
        LSB(TOTAL_DESCRIPTOR_LENGTH),   // wTotalLength (LSB)
        MSB(TOTAL_DESCRIPTOR_LENGTH),   // wTotalLength (MSB)
        0x01,                           // bNumInterfaces
        DEFAULT_CONFIGURATION,          // bConfigurationValue
        0x00,                           // iConfiguration
        C_RESERVED | C_SELF_POWERED,    // bmAttributes
        C_POWER(0),                     // bMaxPowerHello World from Mbed

        INTERFACE_DESCRIPTOR_LENGTH,    // bLength
        INTERFACE_DESCRIPTOR,           // bDescriptorType
        0x00,                           // bInterfaceNumber
        0x00,                           // bAlternateSetting
        0x02,                           // bNumEndpoints
        HID_CLASS,                      // bInterfaceClass
        1,                              // bInterfaceSubClass
        2,                              // bInterfaceProtocol (mouse)
        0x00,                           // iInterface

        HID_DESCRIPTOR_LENGTH,          // bLength
        HID_DESCRIPTOR,                 // bDescriptorType
        LSB(HID_VERSION_1_11),          // bcdHID (LSB)
        MSB(HID_VERSION_1_11),          // bcdHID (MSB)
        0x00,                           // bCountryCode
        0x01,                           // bNumDescriptors
        REPORT_DESCRIPTOR,              // bDescriptorType
        (uint8_t)(LSB(reportDescLength())),        // wDescriptorLength (LSB)
        (uint8_t)(MSB(reportDescLength())),        // wDescriptorLength (MSB)

        ENDPOINT_DESCRIPTOR_LENGTH,     // bLength
        ENDPOINT_DESCRIPTOR,            // bDescriptorType
        PHY_TO_DESC(EPINT_IN),          // bEndpointAddress
        E_INTERRUPT,                    // bmAttributes
        LSB(MAX_PACKET_SIZE_EPINT),     // wMaxPacketSize (LSB)
        MSB(MAX_PACKET_SIZE_EPINT),     // wMaxPacketSize (MSB)
        1,                              // bInterval (milliseconds)

        ENDPOINT_DESCRIPTOR_LENGTH,     // bLength
        ENDPOINT_DESCRIPTOR,            // bDescriptorType
        PHY_TO_DESC(EPINT_OUT),         // bEndpointAddress
        E_INTERRUPT,                    // bmAttributes
        LSB(MAX_PACKET_SIZE_EPINT),     // wMaxPacketSize (LSB)
        MSB(MAX_PACKET_SIZE_EPINT),     // wMaxPacketSize (MSB)
        1,                              // bInterval (milliseconds)
    };
    return configurationDescriptor;
}

static void SetupStageCallback() {
    PCD_HandleTypeDef *hpcd = &hpcd_USB_FS;

    PCD_DBG("hpcd=%p", hpcd);
    PCD_HEX((uint8_t*)hpcd->Setup, 8);
    uint8_t mbRequestType = hpcd->Setup[0] & 0xff;
    uint8_t bRequest = hpcd->Setup[0]>>8 & 0xff;
    uint16_t wValue = hpcd->Setup[0]>>16;
    int wLength = hpcd->Setup[1]>>16;
    data.init(NULL, 0);
    if (mbRequestType == 0x80 && bRequest == 6) {
        uint8_t desc_type = wValue>>8;
        //PCD_DBG("GET_DESCRIPTOR %d, wLength=%d", desc_type, wLength);
        if (desc_type == DEVICE_DESCRIPTOR) {
            data.init(deviceDesc(), deviceDesc()[0]);
        } else if (desc_type == CONFIGURATION_DESCRIPTOR) { 
            data.init(configurationDesc(), TOTAL_DESCRIPTOR_LENGTH);
        } else if (desc_type == STRING_DESCRIPTOR) {
            if ((wValue&0xff) == STRING_OFFSET_LANGID) {
                data.init(stringLangidDesc(), stringLangidDesc()[0]);
            } else if ((wValue&0xff) == STRING_OFFSET_IPRODUCT) {
                data.init(stringIproductDesc(), stringIproductDesc()[0]);
            } else if ((wValue&0xff) == STRING_OFFSET_ISERIAL) {
                data.init(stringIserialDesc(), stringIserialDesc()[0]);
            } else {
                PCD_HEX((uint8_t*)hpcd->Setup, 8);
                PCD_ASSERT(0);
            }
        } else if (desc_type == QUALIFIER_DESCRIPTOR) {
            ; // pass
        } else {
            PCD_HEX((uint8_t*)hpcd->Setup, 8);
            PCD_ASSERT(0);
        }
        int size = std::min(wLength, (int)data.size);
        PCD_DBG("size=%d", size);
        PCD_HEX(data.p, size);
        HAL_PCD_EP_Transmit(hpcd, 0x00, data.p, size);
        seq = SEQ_DATA_IN;
    } else if (mbRequestType == 0x00 && bRequest == 5) { //SET_ADDRESS
        dev_addr = wValue;
        //PCD_DBG("SET_ADDRESS %d", dev_addr);
        HAL_PCD_SetAddress(hpcd, dev_addr);
        HAL_PCD_EP_Transmit(hpcd, 0x00, NULL, 0); // status stage
        seq = SEQ_STATUS_IN;
    } else if (mbRequestType == 0x00 && bRequest == 9) { //SET_CONFIGURATITON
        HAL_PCD_EP_Transmit(hpcd, 0x00, NULL, 0); // status stage
        seq = SEQ_STATUS_IN;
    } else if (mbRequestType == 0x21 && bRequest == 10) { // HID descriptor
        HAL_PCD_EP_Transmit(hpcd, 0x00, NULL, 0); // status stage
        seq = SEQ_STATUS_IN;
    } else if (mbRequestType == 0x81 && bRequest == 6) { // report descriptor
        data.init(reportDesc(), reportDescLength());
        if (data.size < 64) {
            PCD_DBG("size=%d", data.size);
            PCD_HEX(data.p, data.size);
            HAL_PCD_EP_Transmit(hpcd, 0x00, data.p, data.size);
            seq = SEQ_DATA_IN;
        } else {
            PCD_DBG("size=%d", 64);
            PCD_HEX(data.p, 64);
            HAL_PCD_EP_Transmit(hpcd, 0x00, data.p, 64);
            seq = SEQ_DATA_IN_CONTINE;
        }
    } else {
        PCD_HEX((uint8_t*)hpcd->Setup, 8);
        PCD_ASSERT(0);
   }
}

void HAL_PCD_SetupStageCallback(PCD_HandleTypeDef *hpcd) {
    SetupStageCallback();
}

void HAL_PCD_DataOutStageCallback(PCD_HandleTypeDef *hpcd, uint8_t epnum) {
    //PCD_DBG("hpcd=%p, epnum=%d, seq=%d", hpcd, epnum, seq);
    if (epnum == 0) {
        if (seq == SEQ_STATUS_OUT) {
            int r = HAL_PCD_EP_GetRxCount(hpcd, 0x00);
            PCD_ASSERT(r == 0);
        } else {
            PCD_ASSERT(0);
        }
    }
}

__IO bool ep1_done = true;

static void DataInStageCallback(int i) {
    switch(i) {
        case 1: epComplete |= 1UL<<1; break; // IN1EP
    }
}

void HAL_PCD_DataInStageCallback(PCD_HandleTypeDef *hpcd, uint8_t epnum) {
    if (epnum == 0) {
        PCD_DBG("hpcd=%p, epnum=%d, seq=%d", hpcd, epnum, seq);
        switch(seq) {
            case SEQ_DATA_IN:
                HAL_PCD_EP_Receive(hpcd, 0x00, NULL, 0); // status stage
                seq = SEQ_STATUS_OUT;
                break;
            case SEQ_DATA_IN_CONTINE:
                PCD_HEX(data.p + 64, data.size - 64);
                HAL_PCD_EP_Transmit(hpcd, 0x00, data.p + 64, data.size - 64);
                seq = SEQ_DATA_IN;
                break;
            case SEQ_STATUS_IN:
                seq = SEQ_INIT;
                break;
            default:
                PCD_ASSERT(0);
        }
    } else {
        //PCD_DBG("hpcd=%p, epnum=%d, seq=%d", hpcd, epnum, seq);
        ep1_done = true;
    }
    DataInStageCallback(epnum);

}

void HAL_PCD_ResetCallback(PCD_HandleTypeDef *hpcd) {
    PCD_DBG("hpcd=%p", hpcd);
    HAL_PCD_EP_Open(hpcd, 0x00, 64, PCD_EP_TYPE_CTRL);
    HAL_PCD_EP_Open(hpcd, 0x80, 64, PCD_EP_TYPE_CTRL);
    HAL_PCD_EP_Open(hpcd, 0x81, 64, PCD_EP_TYPE_INTR);
}

void HAL_PCD_SuspendCallback(PCD_HandleTypeDef *hpcd) {
    PCD_DBG("hpcd=%p", hpcd);
    if (hpcd->Init.low_power_enable) {
        SCB->SCR |= (uint32_t)((uint32_t)(SCB_SCR_SLEEPDEEP_Msk | SCB_SCR_SLEEPONEXIT_Msk));
    }
}

void HAL_PCD_ResumeCallback(PCD_HandleTypeDef *hpcd) {
    PCD_DBG("hpcd=%p", hpcd);
}

void HAL_PCD_ConnectCallback(PCD_HandleTypeDef *hpcd) {
    PCD_DBG("hpcd=%p", hpcd);
}

void HAL_PCD_DisconnectCallback(PCD_HandleTypeDef *hpcd) {
    PCD_DBG("hpcd=%p", hpcd);
}


void HAL_PCDEx_SetConnectionState(PCD_HandleTypeDef *hpcd, uint8_t state) {
    PCD_DBG("state=%d", state);
    if (state == 1) {
        __HAL_SYSCFG_USBPULLUP_ENABLE();
    } else {
        __HAL_SYSCFG_USBPULLUP_DISABLE();
    } 
}


void L152RE_SystemClock_Config(void) {
    PCD_DBG("");

    RCC_OscInitTypeDef RCC_OscInitStruct;
    RCC_ClkInitTypeDef RCC_ClkInitStruct;

    __PWR_CLK_ENABLE();

    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.HSICalibrationValue = 16;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
    RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
    RCC_OscInitStruct.PLL.PLLDIV = RCC_PLL_DIV3;
    HAL_RCC_OscConfig(&RCC_OscInitStruct);

    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_SYSCLK;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
    HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1);

    __SYSCFG_CLK_ENABLE();
}

uint32_t L152RE_getUSBclock() {
    RCC_OscInitTypeDef cfg;
    HAL_RCC_GetOscConfig(&cfg);
    MBED_ASSERT(cfg.PLL.PLLState == RCC_PLL_ON);
    uint32_t src = (cfg.PLL.PLLSource == RCC_PLLSOURCE_HSI) ? HSI_VALUE : HSE_VALUE;
    MBED_ASSERT(src == 16000000 || src == 8000000);
    switch(cfg.PLL.PLLMUL) {
        case RCC_PLL_MUL3: src *= 3; break;
        case RCC_PLL_MUL4: src *= 4; break;
        case RCC_PLL_MUL6: src *= 6; break;
        case RCC_PLL_MUL8: src *= 8; break;
        case RCC_PLL_MUL12: src *= 12; break;
        case RCC_PLL_MUL16: src *= 16; break;
        case RCC_PLL_MUL24: src *= 24; break;
        case RCC_PLL_MUL32: src *= 32; break;
        case RCC_PLL_MUL48: src *= 48; break;
    }
    return src / 2;
}


void MX_USB_DEVICE_Init(void) {
    PCD_DBG("");

    hpcd_USB_FS.Instance = USB;
    hpcd_USB_FS.Init.dev_endpoints = 8;
    hpcd_USB_FS.Init.speed = PCD_SPEED_FULL;
    hpcd_USB_FS.Init.ep0_mps = DEP0CTL_MPS_8;
    hpcd_USB_FS.Init.phy_itface = PCD_PHY_EMBEDDED;
    hpcd_USB_FS.Init.Sof_enable = DISABLE;
    hpcd_USB_FS.Init.low_power_enable = DISABLE;
    hpcd_USB_FS.Init.battery_charging_enable = DISABLE;
    HAL_PCD_Init(&hpcd_USB_FS);

    HAL_PCDEx_PMAConfig(&hpcd_USB_FS , 0x00 , PCD_SNG_BUF, 0x18);
    HAL_PCDEx_PMAConfig(&hpcd_USB_FS , 0x80 , PCD_SNG_BUF, 0x58);
    HAL_PCDEx_PMAConfig(&hpcd_USB_FS , 0x81 , PCD_SNG_BUF, 0x100);

    HAL_PCD_Start(&hpcd_USB_FS);
}


const int MY_BAUD = 115200;

int main() {
    timestamp.reset();
    timestamp.start();
    pc.baud(MY_BAUD);
    PCD_DBG("%s", __FILE__);
    uint32_t usbclk = L152RE_getUSBclock();
    PCD_DBG("USBclk=%u", usbclk);
    if (usbclk != 48000000) {
        HAL_RCC_DeInit();
        L152RE_SystemClock_Config();
        //SystemCoreClockUpdate();
        pc.baud(MY_BAUD);
    }
    MX_USB_DEVICE_Init();

    wait_ms(2000);

    int32_t angle = 0;
    while (1) {
        if (sw == 0) {
            continue;
        }
        const int32_t radius = 10;
        int16_t x = cos((double)angle*3.14/180.0)*radius;
        int16_t y = sin((double)angle*3.14/180.0)*radius;
        while(x != 0 || y != 0) {
            int dx = x;
            dx = std::min(127, dx);
            dx = std::max(-128, dx);
            x -= dx;
            int dy = y;
            dy = std::min(127, dy);
            dy = std::max(-128, dy);
            y -= dy;
            while(!ep1_done) {
                wait_us(100);
            }
            ep1_done = false;
            uint8_t report[4] = {0x00,dx, dy, 0x00}; // button, x, y, z
            HAL_PCD_EP_Transmit(&hpcd_USB_FS, 0x81, report, sizeof(report));
        }
        angle += 3;
        wait_ms(2);
    }
}

#endif // TEST_PCD5


