#ifdef TEST_BLINKY
#include "mbed.h"

DigitalOut led1(LED1);

int main() {
    while(1) {
        led1 = !led1;
        wait_ms(200);
    }
}
#endif // TEST_BLINKY


