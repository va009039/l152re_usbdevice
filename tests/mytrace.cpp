// mytrace.cpp 2015/6/8
#include "mytrace.h"
myTrace::myTrace():wpos(0) {
    t.reset();
    t.start();
}

void myTrace::trace(const char* func, int line) {
    int i = wpos++ % TRACE_BUF_SIZE;
    buf[i].timestamp_ms = t.read_ms();
    buf[i].func = func;
    buf[i].line = line;
    buf[i].fmt = NULL;
}

void myTrace::trace1(const char* func, int line, const char* fmt, int val) {
    int i = wpos++ % TRACE_BUF_SIZE;
    buf[i].timestamp_ms = t.read_ms();
    buf[i].func = func;
    buf[i].line = line;
    buf[i].fmt = fmt;
    buf[i].val = val;
}

void myTrace::view(const char* func, int line) {
    printf("[%s@%d] --- TRACE VIEW BEGIN ---\n", func, line);
    int rpos = wpos - TRACE_BUF_SIZE;
    rpos = rpos < 0 ? 0 : rpos;
    for(int i = rpos; i < wpos; i++) {
        int p = i % TRACE_BUF_SIZE;
        printf("[%d %dms %s@%d] ", i, buf[p].timestamp_ms, buf[p].func, buf[p].line);
        if (buf[p].fmt != NULL) {
            printf(buf[p].fmt, buf[p].val);
        }
        printf("\n");
   }
   printf("[%s@%d] --- TRACE VIEW END ---\n", func, line);
   clear();
}

void myTrace::assert(const char* func, int line, const char* s) {
    view(func, line);
    printf("[%s@%d] ASSERT!!! %s\n", func, line, s);
    exit(1);
}

myTrace myt;

