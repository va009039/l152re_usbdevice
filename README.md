# L152RE_USBDevice #
NUCLEO-L152REとNUCLEO-F103RBのためのUSBデバイススタック(mbed USBDevice)です。

HAL_PCDドライバで実装しています。

### 制限事項 ###
* アイソクロナス転送の最大パケットサイズは192バイトです。
* USBクロックを48MHzに設定する必要があります。
* NUCLEO-F103RBはソフトコネクトはしていません。USB_DPにプルアップ抵抗が必要です。

### 追加・変更ファイル ###
* USBHAL_STM32L1.cpp - 追加
* USBHAL.h - 変更
* USBEndpoints.h - 変更
* USBEndpoints_STM32L1.h - 追加
