#coding: UTF-8
# striptrace.py 2015/6/15

def strip_trace(fromfile, tofile, f_verbose=False):
    with open(tofile, "w") as dst:
        with open(fromfile) as src:
            for n,line in enumerate(src.readlines()):
                if 'TRACE' in line or 'mytrace.h' in line:
                    pass
                    if f_verbose:
                        print "[@%d] %s" %(n+1, line),
                else:
                    dst.write(line)

def mtime_cmp(fromfile, tofile):
    if not os.path.exists(fromfile):
        return 1
    if os.path.exists(tofile):
        if os.stat(tofile).st_mtime >= os.stat(fromfile).st_mtime:
            return 1
    return 0

if __name__ == "__main__":
    import argparse
    import sys
    import os
    parser = argparse.ArgumentParser()
    parser.add_argument('infiles', nargs='+')
    parser.add_argument("-v", "--verbose", action='store_true')
    args = parser.parse_args()
    fromfile = args.infiles[0]
    tofile = args.infiles[1]

    if mtime_cmp(fromfile, tofile) == 1:
        sys.exit(1)

    strip_trace(fromfile, tofile, args.verbose)
    sys.exit(0)

