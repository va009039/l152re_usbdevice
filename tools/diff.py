#coding: UTF-8
# diff.py 2015/5/6
import sys
import difflib

def diff(fromfile, tofile):
    s1 = open(fromfile).readlines()
    s2 = open(tofile).readlines()
    for line in difflib.unified_diff(s1, s2, fromfile=fromfile, tofile=tofile):
        sys.stdout.write(line)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('infiles', nargs='+')
    args = parser.parse_args()
    fromfile = args.infiles[0]
    tofile   = args.infiles[1]
    diff(fromfile, tofile)
