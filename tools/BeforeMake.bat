rem BeforeMake.bat 2015/6/25
rem
cd c:\dropbox\mbed\L152RE_USBDevice\tools

set PYTHON=c:\python27\python

set BASENAME=USBHAL_STM32L1.cpp
set SRC= ..\DEBUG_USBDevice\USBDevice\%BASENAME%
set DST=..\L152RE_USBDevice\USBDevice\%BASENAME%

echo %SRC%
echo %DST%

%PYTHON% striptrace.py -v %SRC% %DST%
echo %ERRORLEVEL%
