#coding: UTF-8
# arc.py 2015/6/13

if __name__ == "__main__":
    import zipfile
    import os.path

    zipname = 'USBDeviceSTM32L1.zip'
    dirname = os.path.join('..', 'L152RE_USBDevice', 'USBDevice')
    arclists = ['USBEndpoints.h', 'USBEndpoints_STM32L1.h', 'USBHAL.h', 'USBHAL_STM32L1.cpp']

    with zipfile.ZipFile(zipname, 'w') as myzip:
        for arcname in arclists:
            filename = os.path.join(dirname, arcname)
            s = ""
            print "arcname: %s" % arcname
            print "filename: %s" % filename
            with open(filename) as f:
                for n,line in enumerate(f.readlines()):
                    if 'TRACE' in line or 'mytrace.h' in line:
                        pass
                        print "[%s@%d] %s" %(arcname, n+1, line),
                    else:
                        s += line
                #print s
                myzip.writestr(arcname, s)

    with zipfile.ZipFile(zipname) as myzip:
        myzip.printdir()

